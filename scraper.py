import aiohttp
import asyncio
import lxml.html
import json
import sys
import math
import pdb


def async_catch_exception_drop_pdb(func):
    async def decorated(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except Exception as ex:
            print(ex, args, kwargs)
    return decorated

@async_catch_exception_drop_pdb
async def get_movie_meta(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            tree = lxml.html.fromstring(await response.text())
            directors = [
                x.text.strip() for x in tree.xpath('//div[@itemprop="director"]/a/span')
            ]
            actors = [
                x.text.strip() for x in tree.xpath('//div[@itemprop="actor"]/a/span')
            ]
            productions = [
                x.text.strip()
                for x in tree.xpath('//div[@itemprop="productionCompany"]/a/span')
            ]
            description = (
                tree.xpath('//div[@class="vid-details-right"]')[0]
                .xpath("div/p")[0]
                .text.strip()
            )
            return {
                "directors": directors,
                "actors": actors,
                "productions": productions,
                "description": description,
            }


@async_catch_exception_drop_pdb
async def _scrape_movie_page(url, partial_scraped):
    data = await get_movie_meta(url)
    data['movie_id'] = url.split('/')[-1].split('-')[-1]
    partial_scraped.update(data)
    return partial_scraped


@async_catch_exception_drop_pdb
async def _scrape_lom_page(url):
    print(f'scraping {url}')
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            tree = lxml.html.fromstring(await response.text())
            container = tree.xpath('//div[@class="mv-content-items"]')[0]

            tasks = []
            for anchor in container.xpath("a"):
                movie_url = anchor.attrib.get("href")
                poster = anchor.xpath("div/img")[0].attrib.get("src")
                name_with_year = anchor.xpath('div/div/div[@class="mv-desc"]')[
                    0
                ].text.strip()
                res = {}
                res["movie_url"] = movie_url
                res["poster"] = poster
                res["name"] = name_with_year

                tasks.append(
                    asyncio.create_task(_scrape_movie_page(movie_url, res))
                )
            done, pending = await asyncio.wait(tasks)
            print(f'scraping {url} completed')
            res = []
            for task in tasks:
                try:
                    res.append(task.result())
                except Exception as ex:
                    print(ex)
            return res


@async_catch_exception_drop_pdb
async def get_featured_movies(which, frm=1, to=1):
    which_map = {
        "featured": "https://khaanflix.com",
        "indian": "https://khaanflix.com/indian-movies",
        "western": "https://khaanflix.com/western-movies",
    }

    tasks = []
    start = frm

    while start <= to:
        url = f"{which_map[which]}/{start}"
        if which == "featured":
            url = which_map[which]

        tasks.append(asyncio.create_task(_scrape_lom_page(url)))
        start += 1

    done, pending = await asyncio.wait(tasks)

    res = []
    for task in done:
        res += task.result()
    return res


@async_catch_exception_drop_pdb
async def main():
    which = sys.argv[-2]
    page = int(sys.argv[-1])
    i = 0
    data = []

    if page < 40:
        data += await get_featured_movies(which, frm=1, to=page)
    else:
        for j in range(math.ceil(page / 40)):
            frm = ((j * 40) + 1)
            to = ((j + 1) * 40)
            if to > page:
                to = page
            data += await get_featured_movies(which, frm, to)

    with open(f"{which}.json", "w") as f:
        f.write(json.dumps(data))


if __name__ == "__main__":
    asyncio.run(main())
